console.log("hello world");

console.log(document); //the whole HTML document

const txtFirstName = document.querySelector("#txt-first-name");
console.log(txtFirstName); //input field for first name / tag
/*alternate ways:
	> document.getElementById("idName");
	> document.getElementByClassName("className");
	> document.getElementByTagName("tagName");

*/

let spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener('keyup', (event) =>{
	spanFullName.innerHTML = txtFirstName.value;
})
//the value of firstName reflects to fullName
//innerHTML to change the value dynamically

// txtFirstName.addEventListener('keyup', (event) =>{
// 	console.log(event); //will show the keytypes that was use/click in the console log
// 	console.log(event.target) //will show the target value
// 	console.log(event.target.value) //will show the input value
// })

const keyCodeEvent = (e) =>{
	let kc = e.keyCode;
	if (kc === 65) {
		e.target.value = null;
		alert('Someone clicked a');
	}
}

txtFirstName.addEventListener('keyup', keyCodeEvent);




const txtLastName = document.querySelector("#txt-last-name");


const txtFullName = (e)=>{
	let fullName = event.target.value;
	if (fullName !== null){
	 spanFullName.innerHTML = txtFirstName.value + txtLastName.value;
	}
}
txtLastName.addEventListener('keyup', txtFullName);




// const txtLastName = document.querySelector("#txt-last-name");
// console.log(txtLastName);

// txtLastName.addEventListener('keyup', (event) =>{
// 	spanFullName.innerHTML = txtFirstName.value + txtLastName.value;
// })
